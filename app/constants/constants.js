const LOGIN_RESPONSE = 'login_response';
export default { LOGIN_RESPONSE };

module.exports = {
  API_URL: 'http://www.fieldtrack.live/ft/',
  LOGIN_SCREEN: 'LoginScreen',
  ATTENDANCE_SCREEN: 'AttendanceRecordScreenNew',
  SPLASH_INFO_SCREEN: 'SplashInfoScreen',
  SPLASH_SCREEN: 'SplashScreen'
};
