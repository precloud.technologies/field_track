import React, { Component } from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';
import { Content } from 'native-base';
import BackgroundGeolocation from 'react-native-background-geolocation';
import DeviceInfo from 'react-native-device-info';
import { Calendar } from 'react-native-calendars';
import { retrieveItem, storeItem } from '../service/AsyncStorageUtil';
import { DrawerActions, NavigationActions } from 'react-navigation';
import ToolbarHeaderMenuBar from '../components/drawer/ToolbarHeaderMenuBar';
import { Header, InputAlertModal, Confirm, Loader } from '../components/common';
import moment from 'moment';
import postUserLocation from '../service/UpdateLocationService';
import { getData, syncDataToServer, insertData } from '../service/database';

const google = window.google;

class AttendanceRecordScreenNew extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isDialogVisible: false,
            nearBy: null,
            reason: '',
            lastLocation: {
                latitude: '',
                longitude: '',
                odometer: 0.0
            },
            indicatorModalVisible: true,
            type: '',
            checkInDisable: true,
            checkOutDisable: true,
            noRecordFound: true,
            markedDates: {},
            loginTime: '',
            logoutTime: '',
            lastSelectedDate: null,
            loading: false,
            attendaceData: [],
            user_id: '',
            company_id: ''
        };
    }

    componentDidMount() {
        // this.checkCheckinButtonEnabled();
        this.getUserData();
        // this.initialiseLocation();
    }

    initialiseLocation() {
        console.log(DeviceInfo.getModel().replace(/[\s\.,]/g, '-'));
        // 1.  Wire up event-listeners

        // This handler fires whenever bgGeo receives a location update.
        BackgroundGeolocation.onLocation(this.onLocation.bind(this), this.onError);

        // This handler fires when movement states changes (stationary->moving; moving->stationary)
        BackgroundGeolocation.onMotionChange(this.onMotionChange);

        // This event fires when a change in motion activity is detected
        BackgroundGeolocation.onActivityChange(this.onActivityChange.bind(this));

        // This event fires when the user toggles location-services authorization
        BackgroundGeolocation.onProviderChange(this.onProviderChange);
        
        BackgroundGeolocation.onSchedule(this.onSchedule.bind(this));
        
        BackgroundGeolocation.ready({
            reset: true,  // <-- true to always apply the supplied config
            // Geolocation Config
            desiredAccuracy: BackgroundGeolocation.DESIRED_ACCURACY_HIGH,
            distanceFilter: 0,
            // Activity Recognition
            activityRecognitionInterval: 0,
            minimumActivityRecognitionConfidence: 0,
            // pausesLocationUpdatesAutomatically: false,
            // stopTimeout: 1,
            locationUpdateInterval: 1000 * 1, // 3000*60 ms
            fastestLocationUpdateInterval: 1000 * 10, // 3000*60ms
            allowIdenticalLocations: true,
            stopTimeout: 24 * 60,// this is in minute
            deferTime: 0,
            stopDetectionDelay: 0,
            // Application config
            debug: true, // <-- enable this hear sounds for background-geolocation life-cycle.
            logLevel: BackgroundGeolocation.LOG_LEVEL_VERBOSE,
            stopOnTerminate: false,   // <-- Allow the background-service to continue tracking when user closes the app.
            enableHeadless: true,
            startOnBoot: true,        // <-- Auto start tracking when device is powered-up.
            foregroundService: true,
            schedule: [
                '1-7 00:00-23:30'
            ],
            locationAuthorizationAlert: {
                titleWhenNotEnabled: 'location-services not enabled',
                titleWhenOff: 'location-services OFF',
                instructions: 'You must enable "Always" in location-services',
                cancelButton: 'Cancel',
                settingsButton: 'Settings'
            },
            url: 'http://fieldtrack.live:9000/locations',
            params: {               // <-- Optional HTTP params
                "auth_token": "maybe_your_server_authenticates_via_token_YES?",
                'device': {
                    // uuid: DeviceInfo.getModel().replace(/[\s\.,]/g, '-'),
                    // model: DeviceInfo.getModel(),
                    uuid: this.state.user_id,
                    model: this.state.user_id,
                    platform: DeviceInfo.getSystemName(),
                    manufacturer: DeviceInfo.getManufacturer(),
                    version: DeviceInfo.getSystemVersion(),
                    framework: ''
                }
            },
            heartbeatInterval: 10,
            stopOnStationary: false,
            stationaryRadius: 1
        }, (state) => {
            console.log(state);
            this.setState({
                checkInDisable: state.enabled, checkOutDisable: !state.enabled
            });
            this.checkInOutToggleFromLocal(state.enabled, !state.enabled);
        });
    }

    checkCheckinButtonEnabled() {
        retrieveItem('ATTENDANCE_CHECK_IN_DISABLE').then((result, error) => {
            let val = result.checkinDisable;
            console.log(val);
            this.setState({
                checkInDisable: val, checkOutDisable: !val
            });
        }).catch((error) => {
            this.checkInOutToggleFromLocal(false, true);//checkin not disabled
        });
    }

    checkInOutToggleFromLocal(val1, val2) {
        storeItem('ATTENDANCE_CHECK_IN_DISABLE', JSON.stringify({ 'checkinDisable': val1 }))
            .then((res) => {
                this.setState({
                    checkInDisable: val1, checkOutDisable: val2
                });
            }).catch((error) => {
                //console.log(`Promise is rejected with error: ${error}`);
            });
    }

    syncDataFromServer() {
        this.setState({ loading: true });
        syncDataToServer()
            .then((result) => {
                this.setState({ loading: false });
                this.getMonthAttendanceList();
            })
            .catch((error) => {
                this.setState({ loading: false });
                // alert(error);
            });
    }

    getUserData() {
        this.setState({ loading: true });
        getData('table_user')
            .then((res1) => {
                if (res1.length > 0) {
                    this.setState({ user_id: res1.item(0).user_id, company_id: res1.item(0).company_id });
                    this.initialiseLocation();
                    this.getMonthAttendanceList();
                }
            })
            .catch((err) => {
                this.setState({ noRecordFound: true, loading: false });
            })
    }

    getMonthAttendanceList() {
        this.setState({ loading: true });
        getData('attendance')
            .then((result) => {
                if (result.length > 0) {
                    // console.log(result.item(0))
                    let markedCal = {};
                    for (let i = 0; i < result.length; i++) {
                        if (moment(result.item(i).datetime).format('YYYY-MM-DD') in markedCal) {
                            markedCal[moment(result.item(i).datetime).format('YYYY-MM-DD')].data.push(result.item(i));
                        } else {
                            markedCal[moment(result.item(i).datetime).format('YYYY-MM-DD')] = {
                                data: [result.item(i)],
                                marked: true,
                                selected: moment(result.item(i).datetime).format('YYYY-MM-DD') === moment().format('YYYY-MM-DD'),
                                dotColor: '#ff6600'
                            };
                        }
                    }
                    this.setState({ markedDates: markedCal, loading: false });
                    this.dateChange({ dateString: moment().format('YYYY-MM-DD') });
                } else {
                    this.setState({ loading: false });
                }
            }).catch((error) => {
                this.setState({ noRecordFound: true, loading: false });
            });
    }



    getMonthOrDate(value) {
        return value < 10 ? '0' + value : value; // ('' + month) for string result
    }

    startLocation() {
        this.setState({ loading: true });
        BackgroundGeolocation.getCurrentPosition({ persist: false, samples: 1 },
            (position) => {
                getData('table_user')
                    .then((result) => {
                        const lat2 = position.coords.latitude;
                        const lon2 = position.coords.longitude;

                        if (result.item(0).home_lat_long !== '') {
                            const lat1 = result.item(0).home_lat_long.split(',')[0];
                            const lon1 = result.item(0).home_lat_long.split(',')[1];

                            const R = 6371e3; // earth radius in meters
                            const φ1 = lat1 * (Math.PI / 180);
                            const φ2 = lat2 * (Math.PI / 180);
                            const Δφ = (lat2 - lat1) * (Math.PI / 180);
                            const Δλ = (lon2 - lon1) * (Math.PI / 180);

                            const a = (Math.sin(Δφ / 2) * Math.sin(Δφ / 2)) +
                                ((Math.cos(φ1) * Math.cos(φ2)) * (Math.sin(Δλ / 2) * Math.sin(Δλ / 2)));

                            const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

                            const distance = R * c;
                            if (distance < 200) {
                                this.setState({ isDialogVisible: true, nearBy: 'Near By Home', loading: false });
                            }
                        }

                        // if office lat-lng != ''
                        if (result.item(0).office_lat_long !== '') {

                            const lat3 = result.item(0).office_lat_long.split(',')[0];
                            const lon3 = result.item(0).office_lat_long.split(',')[1];

                            const R = 6371e3; // earth radius in meters
                            const φ1 = lat3 * (Math.PI / 180);
                            const φ2 = lat2 * (Math.PI / 180);
                            const Δφ = (lat2 - lat3) * (Math.PI / 180);
                            const Δλ = (lon2 - lon3) * (Math.PI / 180);

                            const a = (Math.sin(Δφ / 2) * Math.sin(Δφ / 2)) +
                                ((Math.cos(φ1) * Math.cos(φ2)) * (Math.sin(Δλ / 2) * Math.sin(Δλ / 2)));

                            const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

                            const distance = R * c;
                            if (distance > 200) {
                                this.setState({ isDialogVisible: true, nearBy: 'Out Of Office', loading: false });
                            }

                            if (!this.state.isDialogVisible) {
                                this.setState({ loading: false });
                                this.backgroundLocationStart();
                            }
                        }
                    })
                    .catch((error) => {
                        this.setState({ loading: false });
                    })
            },
            (error) => {
                console.log(error);
                alert('Please turn on GPS');
                this.setState({ loading: false });
            });
    }

    backgroundLocationStart() {
        BackgroundGeolocation.startSchedule((state) => {
            console.log('[startSchedule] success: ', state);
            this.setState({ type: 'login' });
            this.checkInOutToggleFromLocal(true, false);
        });
    }
    
    stopLocation() {
        this.setState({ loading: true });
        BackgroundGeolocation.getCurrentPosition({ persist: false, samples: 1 },
            (position) => {
                getData('table_user')
                    .then((result) => {
                        const lat2 = position.coords.latitude;
                        const lon2 = position.coords.longitude;

                        if (result.item(0).home_lat_long !== '') {
                            const lat1 = result.item(0).home_lat_long.split(',')[0];
                            const lon1 = result.item(0).home_lat_long.split(',')[1];

                            const R = 6371e3; // earth radius in meters
                            const φ1 = lat1 * (Math.PI / 180);
                            const φ2 = lat2 * (Math.PI / 180);
                            const Δφ = (lat2 - lat1) * (Math.PI / 180);
                            const Δλ = (lon2 - lon1) * (Math.PI / 180);

                            const a = (Math.sin(Δφ / 2) * Math.sin(Δφ / 2)) +
                                ((Math.cos(φ1) * Math.cos(φ2)) * (Math.sin(Δλ / 2) * Math.sin(Δλ / 2)));

                            const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

                            const distance = R * c;
                            if (distance < 200) {
                                this.setState({ isDialogVisible: true, nearBy: 'Near By Home', loading: false });
                            }
                        }

                        // if office lat-lng != ''
                        if (result.item(0).office_lat_long !== '') {

                            const lat3 = result.item(0).office_lat_long.split(',')[0];
                            const lon3 = result.item(0).office_lat_long.split(',')[1];

                            const R = 6371e3; // earth radius in meters
                            const φ1 = lat3 * (Math.PI / 180);
                            const φ2 = lat2 * (Math.PI / 180);
                            const Δφ = (lat2 - lat3) * (Math.PI / 180);
                            const Δλ = (lon2 - lon3) * (Math.PI / 180);

                            const a = (Math.sin(Δφ / 2) * Math.sin(Δφ / 2)) +
                                ((Math.cos(φ1) * Math.cos(φ2)) * (Math.sin(Δλ / 2) * Math.sin(Δλ / 2)));

                            const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

                            const distance = R * c;
                            if (distance > 200) {
                                this.setState({ isDialogVisible: true, nearBy: 'Out Of Office', loading: false });
                            }

                            if (!this.state.isDialogVisible) {
                                // this.setState({ loading: false });
                                this.stopBackgroundLocation();
                            }
                        }
                    })
                    .catch((error) => {
                        this.setState({ loading: false });
                    })
            },
            (error) => {
                console.log(error);
                alert('Please turn on GPS');
                this.setState({ loading: false });
            });
    }

    stopBackgroundLocation(){
        let query = `INSERT INTO attendance (company_id, date, datetime, attendance_id, lat, logoutby, longi, odometer, remark, time, type, user_id, utctime, modify, push_flag, geofence) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`;
        let value = [this.state.company_id, moment().format('YYYY-MM-DD'), moment().format('YYYY-MM-DD hh:mm:ss'), '', this.state.lastLocation.latitude, 'user', this.state.lastLocation.longitude, this.state.lastLocation.odometer, this.state.reason, moment().format('hh:mm:ss'), 'logout', this.state.user_id, '+05.30', true, true, this.state.nearBy];
        insertData(query, value)
            .then((result) => {
                console.log(result);
                this.setState({ reason: '', loading: false });
                this.getMonthAttendanceList();
                syncDataToServer();
            })
            .catch((error) => {
                console.log(error);
                this.setState({ loading: false });
            });
        updateLocationToServer(
            this.state.lastLocation.latitude,
            this.state.lastLocation.longitude,
            'logout',
            this.state.lastLocation.odometer,
            this.state.user_id,
            this.state.company_id
        );
        BackgroundGeolocation.stopSchedule((state) => {
            console.log(state);
            if (state.enabled) {
                BackgroundGeolocation.stop();
                // this.checkInOutToggleFromLocal(false, true);
            }
            this.checkInOutToggleFromLocal(false, true);
        });
    }

    onLocation(location) {
        console.log(location);
        this.setState({
            lastLocation: {
                latitude: location.coords.latitude,
                longitude: location.coords.longitude,
                odometer: location.odometer
            }
        });
        // console.log(this.state.type);
        if (this.state.type === 'login') {
            if ((location.coords.latitude != null || location.coords.latitude !== undefined || location.coords.latitude !== '') &&
                (location.coords.longitude != null || location.coords.longitude !== undefined || location.coords.longitude !== '')) {
                //if location is available then login values are sent else login state will not change
                updateLocationToServer(location.coords.latitude, location.coords.longitude, this.state.type, location.odometer, this.state.user_id, this.state.company_id);
                let query = `INSERT INTO attendance (company_id, date, datetime, attendance_id, lat, logoutby, longi, odometer, remark, time, type, user_id, utctime, modify, push_flag, geofence) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`;
                let value = [this.state.company_id, moment().format('YYYY-MM-DD'), moment().format('YYYY-MM-DD hh:mm:ss'), '', location.coords.latitude, '', location.coords.longitude, location.odometer, this.state.reason, moment().format('hh:mm:ss'), this.state.type, this.state.user_id, '+05.30', true, true, this.state.nearBy];
                insertData(query, value)
                    .then((result) => {
                        this.setState({ reason: '' });
                        this.getMonthAttendanceList();
                        syncDataToServer();
                    })
                    .catch((error) => {
                        this.setState({ loading: false });
                    });
                this.setState({ type: '' });// next api call to update location  do not send login key
            }
        } else {
            updateLocationToServer(location.coords.latitude, location.coords.longitude, this.state.type, location.odometer, this.state.user_id, this.state.company_id);
        }
    }

    onError(error) {
        //console.warn('[location] ERROR -', error);
    }

    onSchedule(state) {
        console.log(state);
        if (!state.enabled) {
            BackgroundGeolocation.setOdometer(0).then((location) => {
                // This is the location where odometer was set at.
                console.log('[setOdometer] success: ', location);
                this.checkInOutToggleFromLocal(false, true);
            });
            this.stopLocation();
        }
    }

    // onEnabledChange(state) {
    //     console.log(state);
    //     if (!state) {
    //         this.stopLocation();
    //     }
    // }

    onActivityChange(event) {
        if (event.activity === 'still') {
            updateLocationToServer(
                this.state.lastLocation.latitude,
                this.state.lastLocation.longitude,
                this.state.type,
                this.state.lastLocation.odometer,
                this.state.user_id,
                this.state.company_id
            );
        }
    }

    onProviderChange(provider) {
        //console.log('[providerchange] -', provider.enabled, provider.status);
    }

    onMotionChange(event) {
        //console.log('[motionchange] -', event.isMoving, event.location);
    }

    // You must remove listeners when your component unmounts
    componentWillUnmount() {
        // BackgroundGeolocation.removeListeners();
    }


    openHomeDrawer() {
        this.props.navigation.dispatch(DrawerActions.openDrawer());
    }

    renderAlert() {
        if (this.state.isDialogVisible) {
            return (
                <InputAlertModal
                    children={`You are ${this.state.nearBy}`}
                    value={this.state.reason}
                    onChangeText={(reason) => this.setState({ reason })}
                    onAccept={() => {
                        this.setState({ isDialogVisible: false });
                        if(this.state.checkOutDisable){
                            this.backgroundLocationStart();
                        }else{
                            this.stopBackgroundLocation();
                        }
                    }}
                    onDecline={() => {
                        this.setState({ isDialogVisible: false });
                    }}
                />
            )
        }
    }

    dateChange(day) {
        const d = day.dateString;
        if (d in this.state.markedDates) {
            //remove old selected date true
            const oldValueMap = this.state.markedDates;
            const newMap = {};
            Object.entries(oldValueMap).forEach(([key, value]) => {
                newMap[key] = {
                    data: value.data,
                    login: value.login,
                    logout: value.logout,
                    marked: value.marked,
                    selected: key === d
                };
            });
            //set new date info to show details
            const selectedCalObject = this.state.markedDates[d];
            this.setState({
                noRecordFound: !selectedCalObject.marked,//if date marked then only show login/logout
                loginTime: selectedCalObject.login,
                logoutTime: selectedCalObject.logout,
                lastSelectedDate: d,
                markedDates: newMap,
                attendaceData: selectedCalObject.data
            });
        } else {
            //remove old selected date true
            const oldValueMap = this.state.markedDates;
            const newMap = {};
            Object.entries(oldValueMap).forEach(([key, value]) => {
                newMap[key] = {
                    data: value.data,
                    login: value.login,
                    logout: value.logout,
                    marked: value.marked,
                    selected: false
                };
            });
            //set selected for new date
            newMap[d] = {
                login: '',
                logout: '',
                marked: false,
                selected: true
            };
            this.setState({ noRecordFound: true, loginTime: '', logoutTime: '', markedDates: newMap, attendaceData: [] });
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Loader loading={this.state.loading} />
                <ToolbarHeaderMenuBar
                    headerText="Attendance"
                    call={this.openHomeDrawer.bind(this)}
                    notificationScreen={() => {
                        this.props.navigation.dispatch(NavigationActions.navigate({ routeName: 'NotificationScreen' }));
                    }}
                    syncData={() => {
                        this.setState({ confirmToSync: true });
                    }}
                    navigation={this.props}
                />
                <View style={styles.btnContainer}>
                    <Button
                        style={styles.buttonLeft}
                        disabled={this.state.checkInDisable}
                        title="Check In"
                        onPress={() => this.startLocation()}
                    />
                    <Button
                        style={styles.buttonRight}
                        title="Check Out"
                        disabled={this.state.checkOutDisable}
                        onPress={() => {
                            // if((this.state.nearBy === 'Home' || this.state.nearBy === 'Office') && this.state.reason === ''){
                            //     this.setState({ isDialogVisible: true });
                            // }else{
                            this.stopLocation();
                            // }
                        }
                        }
                    />
                </View>

                {/*<Text style={styles.headerStyle}>Attendance History</Text>*/}
                <Header headerText={'Attendance History'} />
                <Calendar
                    // Initially visible month. Default = Date()
                    //current={'2012-03-01'}
                    // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
                    //minDate={'2012-05-10'}
                    // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
                    // maxDate={'2019-01-14'}
                    // Handler which gets executed on day press. Default = undefined
                    onDayPress={(day) => {
                        this.dateChange(day);
                    }}
                    // Handler which gets executed on day long press. Default = undefined
                    onDayLongPress={(day) => {
                        this.dateChange(day);
                    }}
                    // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
                    monthFormat={'MMMM yyyy'}
                    // Handler which gets executed when visible month changes in calendar. Default = undefined
                    onMonthChange={(m) => {
                        // this.getMonthAttendanceList(m.month, m.year);
                    }}
                    // Hide month navigation arrows. Default = false
                    hideArrows={false}
                    // Replace default arrows with custom ones (direction can be 'left' or 'right')
                    //renderArrow={(direction) => (<Arrow/>)}
                    // Do not show days of other months in month page. Default = false
                    hideExtraDays={true}
                    // If hideArrows=false and hideExtraDays=false do not switch month when tapping on greyed out
                    // day from another month that is visible in calendar page. Default = false
                    disableMonthChange={true}
                    // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
                    firstDay={1}
                    // Hide day names. Default = false
                    hideDayNames={false}
                    // Show week numbers to the left. Default = false
                    showWeekNumbers={false}
                    // Handler which gets executed when press arrow icon left. It receive a callback can go back month
                    onPressArrowLeft={substractMonth => substractMonth()}
                    // Handler which gets executed when press arrow icon left. It receive a callback can go next month
                    onPressArrowRight={addMonth => addMonth()}

                    markedDates={this.state.markedDates}//format  {'2018-12-17': { marked: true }}

                    // Specify style for calendar container element. Default = {}
                    style={styles.calender}
                    // Specify theme properties to override specific styles for calendar parts. Default = {}
                    theme={{
                        textSectionTitleColor: '#b6c1cd',
                        selectedDayBackgroundColor: '#ff6600',
                        selectedDayTextColor: '#ffffff',
                        todayTextColor: '#00adf5',
                        dayTextColor: '#2d4150',
                        textDisabledColor: '#d9e1e8',
                        dotColor: '#00adf5',
                        selectedDotColor: '#ffffff',
                        arrowColor: 'orange',
                        monthTextColor: '#ff6600',
                        textDayFontFamily: 'monospace',
                        textMonthFontFamily: 'monospace',
                        textDayHeaderFontFamily: 'monospace',
                        textMonthFontWeight: 'bold',
                        textDayFontSize: 14,
                        textMonthFontSize: 14,
                        textDayHeaderFontSize: 14
                    }}
                />
                <Content>
                    {/* <Card style={{ marginBottom: 10 }}> */}
                    {this.state.noRecordFound
                        ?
                        <Header headerText={'No Events'} />
                        :
                        <View style={{ alignItems: 'center', backgroundColor: '#fff', borderColor: '#333', borderWidth: 1, borderRadius: 5, margin: 5 }}>
                            {this.state.attendaceData.map((item, index) => {
                                return (
                                    <View key={index}>
                                        {item.type === 'login' ?
                                            <Text style={{ fontSize: 14, margin: 5, fontWeight: '500' }}>Login: {item.time}</Text>
                                            :
                                            <Text style={{ fontSize: 14, margin: 5, fontWeight: '500' }}>Logout: {item.time}</Text>
                                        }
                                    </View>
                                )
                            })}
                        </View>
                    }
                    {/* </Card> */}

                    {this.renderAlert()}
                    {this.state.confirmToSync && <Confirm
                        onAccept={() => {
                            this.syncDataFromServer();
                            this.setState({ confirmToSync: false });
                        }}
                        onDecline={() => {
                            this.setState({ confirmToSync: false });
                        }}
                    >{'Do you want to Refresh page?'}</Confirm>}
                </Content>
            </View>
        );
    }
}

function updateLocationToServer(lat, lng, type, odometer, user_id, company_id) {
    postUserLocation(lat, lng, type, odometer, user_id, company_id)
        .then(res => {
            console.log('response', res);
        })
        .catch(err => {
            // console.log('response error', err);
        });
};


AttendanceRecordScreenNew.navigationOptions = {
    title: 'Attendance Record Log'
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#eee',
        flexDirection: 'column'
    },
    btnContainer: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        marginTop: 15,
        marginBottom: 5
    },
    buttonRight: {
        marginTop: 10,
        marginBottom: 10,
        padding: 10,
        alignSelf: 'flex-end'
    },
    buttonLeft: {
        marginTop: 10,
        marginBottom: 10,
        padding: 10,
        alignSelf: 'flex-start'
    },
    headerStyle: {
        fontSize: 15,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        marginBottom: 10,
        marginTop: 10
    },
    calender: {
        borderTopWidth: 1,
        paddingTop: 5,
        borderBottomWidth: 1,
        borderColor: '#eee',
        height: 300
    }
});

export default AttendanceRecordScreenNew;
