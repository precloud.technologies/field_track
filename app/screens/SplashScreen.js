/* eslint-disable global-require */
import React from 'react';
import { Image, StyleSheet, View, Platform } from 'react-native';
import FCM, { FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType, NotificationActionType, NotificationActionOption, NotificationCategoryOption } from "react-native-fcm";
import { retrieveItem } from '../service/AsyncStorageUtil';
import { createDatabase } from '../service/database';

class SplashScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            navigation: props.navigation
        };
        createDatabase()
            .then((result) => {
                //console.log(result);
            })
            .catch((error) => {
                //console.log(error);
            });
    }

    componentWillMount() {
        const { navigation } = this.state;
        setTimeout(() => {
            retrieveItem('IS_LOGIN').then((result) => {
                if (result) {
                    navigation.replace('AppDrawerNavigator')
                } else {
                    navigation.replace('LoginScreen');
                }
            }).catch((error) => {
                navigation.replace('LoginScreen');
            });
        }, 1000);
    }

    componentDidMount() {
        FCM.requestPermissions();
        // FCM.getInitialNotification().then(notif => {
        //     console.log("INITIAL NOTIFICATION", notif)
        // });

        FCM.on(FCMEvent.Notification, notif => {
            console.log("Notification", notif);
            // this.sendRemote(notif);
            if (Platform.OS === 'android' && notif._notificationType === NotificationType.WillPresent && !notif.local_notification) {
                // this notification is only to decide if you want to show the notification when user if in foreground.
                // usually you can ignore it. just decide to show or not.
                notif.finish(WillPresentNotificationResult.All)
                return;
            }

            if (notif.opened_from_tray) {
                if (notif.targetScreen === 'detail') {
                    setTimeout(() => {
                        navigation.navigate('Detail')
                    }, 500)
                }
                setTimeout(() => {
                    alert(`User tapped notification\n${JSON.stringify(notif)}`)
                }, 500)
            }
        });
        // // This method give received notifications to mobile to display.
        // this.notificationUnsubscribe = FCM.on("notification", notif => {
        //     console.log("a", notif);
        //     if (notif && notif.local_notification) {
        //         return;
        //     }
        //     this.sendRemote(notif);
        // });

        // // this method call when FCM token is update(FCM token update any time so will get updated token from this method)
        // this.refreshUnsubscribe = FCM.on("refreshToken", token => {
        //     console.log("TOKEN (refreshUnsubscribe)", token);
        //     this.props.onChangeToken(token);
        // });
    }

    // This method display the notification on mobile screen.
    sendRemote(notif) {
        console.log('send');
        FCM.presentLocalNotification({
            title: notif.title,
            body: notif.body,
            priority: "high",
            click_action: notif.click_action,
            show_in_foreground: true,
            local: true
        });
    }
    componentWillUnmount() {
        // this.refreshUnsubscribe();
        // this.notificationUnsubscribe();
    }

    render() {

        return <View style={styles.container} >
            <Image
                style={styles.app_logo}
                source={require('../assets/splash.png')}
                resizeMode="cover"
            />
        </View >;

    }
}

SplashScreen.navigationOptions = {
    header: null
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    app_logo: {
        flex: 1,
        height: undefined,
        width: undefined
    }
});

export default SplashScreen;
