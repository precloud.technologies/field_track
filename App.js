import React from 'react';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import AppReducer from './app/reducers';
import { AppContainer } from './app/components/AppNavigator';

const store = createStore(AppReducer, applyMiddleware(thunk));

const App = () =>
    (
        <Provider store={store}>
            <AppContainer/>
        </Provider>
    );


export default App;